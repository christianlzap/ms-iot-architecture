package com.tharsis.response;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author christian
 */
@Getter
@Setter
public class SingleResponse {

    private String message;
    private Object result;
}

package com.tharsis.persist;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author christian
 * @param <T>
 * @param <K>
 */
public abstract class RepositoryJPA<T, K extends Serializable> implements GenericCrud<T, K> {

    @PersistenceContext
    private EntityManager em;

    private Class<T> type;

    /**
     * Persiste una Entidad
     *
     * @param entity
     * @return
     */
    @Override
    //@Transactional
    public T add(T entity) {
        try {
            beginTransaction();
            em.persist(entity);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransaction();
        }
        return entity;
    }

    /**
     * Actualiza una entidad
     *
     * @param entity
     * @return
     */
    @Override
    public T update(T entity) {
        try {
            beginTransaction();
            em.merge(entity);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransaction();
        }
        return entity;
    }

    /**
     * Elimina una Entidad
     *
     * @param entity
     * @return
     */
    @Override
    public T delete(T entity) {
        try {
            beginTransaction();
            //em.remove(entity);
            em.remove(em.contains(entity) ? entity : em.merge(entity));
            commitTransaction();
        } catch (Exception e) {
            rollbackTransaction();
        }
        return entity;
    }

    /**
     * Obtiene una Entidad en base al ID
     *
     * @param entity
     * @param id
     * @return
     */
    @Override
    public T findById(Class<T> entity, K id) {
        return em.find(entity, id);
    }

    /**
     * Obtiene todos los datos en base a un consulta de tipo NameQuery
     *
     * @param namedQuery
     * @return
     */
    @Override
    public List<T> findAll(String namedQuery) {
        List<T> list = em.createNamedQuery(namedQuery, type).getResultList();
        return list;
    }

    /**
     * Obtiene todos los datos en base a un consulta de tipo NameQuery y Número
     * de resultados
     *
     * @param namedQuery
     * @param results
     * @return
     */
    @Override
    public List<T> findAll(String namedQuery, int results) {
        Query query = em.createNamedQuery(namedQuery).setMaxResults(results);
        List<T> list = query.getResultList();
        return list;
    }

    /**
     *
     * @param iQuery
     * @param param
     * @return
     */
    @Override
    public Query createQuery(String iQuery, Map<String, Object> param) {
        Query query = em.createQuery(iQuery);
        if (param != null) {
            for (Map.Entry<String, Object> entry : param.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query;
    }

    /**
     *
     * @param iQuery
     * @param param
     * @return
     */
    @Override
    public Query createNativeQuery(String iQuery, Map<String, Object> param) {
        Query query = em.createNativeQuery(iQuery);
        if (param != null) {
            param.entrySet().forEach((entry) -> {
                query.setParameter(entry.getKey(), entry.getValue());
            });
        }
        return query;
    }

    /**
     * Devuelve una consulta en bas a un NamedQuery y parametros de consulta
     *
     * @param iQuery
     * @param param
     * @return
     */
    @Override
    public Query createNamedQuery(String iQuery, Map<String, Object> param) {
        Query query = em.createNamedQuery(iQuery);
        if (param != null) {
            param.entrySet().forEach((entry) -> {
                query.setParameter(entry.getKey(), entry.getValue());
            });
        }
        return query;
    }

    public <X> TypedQuery<X> createQuery(String jpql, Map<String, Object> param, Class<X> entityClass) {
        TypedQuery<X> query = em.createQuery(jpql, entityClass);
        if (param != null) {
            for (Map.Entry<String, Object> entry : param.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query;
    }

    public <X> TypedQuery<X> createNamedQuery(String namedQuery, Map<String, Object> map, Class<X> entityClass) {
        TypedQuery<X> query = em.createNamedQuery(namedQuery, entityClass);
        if (map != null) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query;
    }

    private void beginTransaction() {
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
    }

    private void commitTransaction() {
        if (em.getTransaction().isActive()) {
            em.getTransaction().commit();
        }
    }

    private void rollbackTransaction() {
        if (em.getTransaction().isActive()) {
            em.getTransaction().rollback();
        }
    }

}

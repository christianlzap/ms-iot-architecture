package com.tharsis.persist;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

/**
 *
 * @author christian
 * @param <T> Type Object
 * @param <K> Key Object
 */
public interface GenericCrud <T, K extends Serializable> {
    T add(T entity);
    T update(T entity);
    T delete(T entity);
    T findById(Class<T> entity, K id);
    List<T> findAll(String namedQuery);
    List<T> findAll(String namedQuery, int results);
    
    Query createQuery(String iQuery, Map<String, Object> param);
    Query createNativeQuery(String iQuery, Map<String, Object> param);
    Query createNamedQuery(String iQuery, Map<String, Object> param);
}

package com.tharsis.scheduler;

import java.util.HashMap;
import java.util.Map;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author christian
 */
public class ExecuteJob {
    /**
     * Método que ejecuta una tarea respectiva
     * @param dtoJob 
     */
    public static void runJob(JobVO dtoJob) {
        try {
            JobDetail jobDetail = JobBuilder.newJob(dtoJob.getJobClass()).withIdentity(dtoJob.getIdentity()).build();
            Trigger trigger = null;
            Map<Frequency, Object> map = new HashMap<>();
            map.put(dtoJob.getFrequency(), map);

            if (map.containsKey(Frequency.SECONDS)) {
                trigger = TriggerBuilder.newTrigger()
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(dtoJob.getTime()).repeatForever())
                        .build();
            }

            if (map.containsKey(Frequency.MINUTES)) {
                trigger = TriggerBuilder.newTrigger()
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(dtoJob.getTime()).repeatForever())
                        .build();
            }
            if (map.containsKey(Frequency.HOURS)) {
                trigger = TriggerBuilder.newTrigger()
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(dtoJob.getTime()).repeatForever())
                        .build();
            }
            SchedulerFactory schedulerFactory = new StdSchedulerFactory();
            Scheduler scheduler = schedulerFactory.getScheduler();
            scheduler.start();
            scheduler.scheduleJob(jobDetail, trigger);

        } catch (SchedulerException e) {
            
        }
    }

}

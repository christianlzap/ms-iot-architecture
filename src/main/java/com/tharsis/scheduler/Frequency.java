package com.tharsis.scheduler;

/**
 *
 * @author christian
 */
public enum Frequency {
    SECONDS,
    MINUTES,
    HOURS
}

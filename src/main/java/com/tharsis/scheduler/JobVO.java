package com.tharsis.scheduler;

import org.quartz.Job;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author christian
 */
@Getter
@Setter
public class JobVO {
    private String identity;
    private Integer time;
    private Class<? extends Job> jobClass;
    private Frequency frequency;    
}

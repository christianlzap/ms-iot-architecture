package com.tharsis.util;

import java.util.List;

/**
 *
 * @author christian
 */
public class UtilCollection {

    public static final int FIRST_ELEMENT = 0;

    /**
     * Verfica si una lista esta vacia
     * @param list
     * @return 
     */
    public static Boolean isEmptyList(List<?> list) {
        return (list == null || list.isEmpty());
    }

    /**
     * Obtiene el primer elemento de una lista
     * @param <E>
     * @param list
     * @return 
     */
    public static <E> E firstElement(List<E> list) {
        E primerElemento = null;
        if (!isEmptyList(list)) {
            primerElemento = list.get(FIRST_ELEMENT);
        }
        return primerElemento;
    }

}

package com.tharsis.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author christian
 */
public class UtilDate {

    public static final String FORMAT_DATE = "yyyy-MM-dd";
    public static final String FORMAT_HOUR = "HH:mm:ss";

    public static Date formatDate(Date date) {
        DateFormat format = new SimpleDateFormat(FORMAT_DATE);
        Date newDate = null;
        try {
            newDate = format.parse(format.format(date));
        } catch (ParseException ex) {
        }
        return newDate;
    }
    
    public static boolean compareTwoDates(Date date1, Date date2) {
        if(formatDate(date2).equals(date2))
            return true;
        return false;
    }
    
    public static Date getDateNow() {
        return formatDate(new Date());
    }
    
    public static Date getHourNow(){
        return formatDateToHour(new Date());
    }
    
    public static Date formatDateToHour(Date date){
        DateFormat format = new SimpleDateFormat(FORMAT_DATE);
        Date newDate = null;
        try {
            newDate = format.parse(format.format(date));
        } catch (ParseException ex) {
        }
        return newDate;
    }
    
}

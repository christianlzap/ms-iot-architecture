package com.tharsis.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author christian
 */
public class UtilEncrypt {

    public static String encryptToSha1(String text){
        return DigestUtils.sha1Hex(text);
    }
    
    public static String encryptToMd5(String text){
        return DigestUtils.md5Hex(text);
    }
    
}

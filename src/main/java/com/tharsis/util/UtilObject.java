package com.tharsis.util;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;

/**
 *
 * @author christian
 */
public class UtilObject {

    public static <O, D> D objeto(O objetoOrigen, Class<D> objetoDestino) {
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();
        return mapper.map(objetoOrigen, objetoDestino);
    }
}
